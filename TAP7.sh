#!/bin/bash

 #Colors
black="\033[1;37m"
grey="\033[0;37m"
purple="\033[0;35m"
red="\033[1;31m"
green="\033[1;32m"
yellow="\033[1;33m"
Purple="\033[0;35m"
Cyan="\033[0;36m"
Cafe="\033[0;33m"
Fiuscha="\033[0;35m"
blue="\033[1;34m"
transparent="\e[0m"

function banner(){
printf " $blue                                        _   _    _  _____ ____         ___  _   _ "$transparent"\n";
printf " $blue                                       | | | |  / \|_   _/ ___|       / _ \| \ | |"$transparent"\n";
printf " $blue                                       | |_| | / _ \ | | \___ \ _____| | | |  \| |"$transparent"\n";
printf "  $blue                                      |  _  |/ ___ \| |  ___) |_____| |_| | |\  |"$transparent"\n";
printf "            $blue                            |_| |_/_/   \_\_| |____/       \___/|_| \_|"$transparent"\n";
printf "                                                                                                    ";
printf "     $yellow                                                                   [Version 1.1.0]                     "$transparent"\n"; 
printf "\e[0;32m                                                       Presented By:-                     "$transparent"\n";
printf ""$red"                                                 TAPs - The Ace Programmings                                  "$transparent"\n";
printf "                                                                                     "$transparent"\n";
         
}
#define dependency check
function dependency_check() {
clear
banner
sleep 3
echo -e ""$yellow"                                               Checking for installed tools"
echo
sleep 3
 #define tor dependency check
if [ -z "$(ls -A /usr/share/tor/)" ]; then
   echo -e  ""$red"[x] Tor is not installed"
   echo -e  "install it by typing 'apt-get install tor'"
   echo -e 
   sleep 3
   exit
else
   sleep 3
   echo -e  ""$green"[+] Tor is installed!"
   echo -e 
   sleep 3
fi
#define proxychains dependency check
if [ -z "$(ls -A /usr/bin/proxychains)" ]; then
   echo -e  ""$red"[x] proxychains doesn't exit to your system"
   echo -e  "install it by typing 'apt-get install proxychains'"
   echo -e 
   sleep 3
   exit
else
   echo -e  ""$green"[+] proxychains is installed!"
   echo -e 
   sleep 3
fi
#define sed dependency check 
   if [ -z "$(ls -A /usr/bin/sed)" ]; then
   echo -e  ""$red"[x] sed doesn't exit to your system!"
   echo -e  "install it by typing 'apt-get install sed'"
   echo -e 
   sleep 3
   exit
else
   echo -e  ""$green"[+] sed is installed!"
   echo -e 
   sleep 3
fi
clear 
}
#backup function
function backup(){
mkdir $HOME/tbackup &> a.txt

cp /etc/proxychains.conf $HOME/tbackup
}
#restore function
function equilibrium(){
backup
rm -rv /etc/proxychains.conf
cp $HOME/tbackup/proxychains.conf /etc/
rm -rv tbackup &> a.txt
}
#submain function
function submain(){
echo "setting-up chains..."
echo
sleep 2
sed -i -e 's/#dynamic_chain/dynamic_chain/g' /etc/proxychains.conf
sed -i -e 's/strict_chain/#strict_chain/g' /etc/proxychains.conf
echo "adding custom network"
echo
sleep 3
echo socks5	127.0.0.1 9050 >> /etc/proxychains.conf
}
#main function
function main(){
clear
banner

echo "type 'help' for help"
echo
sleep 3 
x=$(echo -e "\e[4mTAP7\e[0m"">")
read -r -p $x str
case $str in
   help)
     echo "commands: start, stop, set browser, quit, help"
     sleep 3
     echo
     ;;
   start)
     echo "starting..."
     sleep 2
     echo
     backup
     submain
     echo "you are in anonymous mode now"
     sleep 3
     echo
     read -r -p "want to start surfing?" choice 
     echo
     sleep 3
     if [ "$choice" = "y" ];
     then
     echo "opening..."
     command proxychains firefox-esr
     command service tor stop
     equilibrium
     else
     echo "ok!"
     equilibrium
     fi
     ;;
   stop)
     sleep 3
     echo "stopping..."
     command service tor stop
     equilibrium
     ;;
   browser)
    read -r -p "Enter browser name here:" br
    sleep 2
    read -r -p "want to start surfing?" choice 
    sleep 3
    if [ "$choice" = "y" ];
    then
    echo "opening..."
    backup
    submain
    command proxychains $br
    command service tor stop
    equilibrium
    else
    echo "ok!"
    equilibrium
    fi
    ;;
   quit)
   exit
   ;;
   *)
     echo "Enter proper command please!"
     equilibrium    
     ;;
esac
sleep 4
main
}
#start
dependency_check
main
